package com.maxworld.contactslist.config;

public class Constants {

    public static final int CONTACTS_LOADER_ID = 0;
    public static final int REQUEST_CALL_PERMISSIONS = 1;
    public static final int REQUEST_CONTACTS_PERMISSIONS = 2;
}
