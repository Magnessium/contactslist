package com.maxworld.contactslist.activity;

import android.Manifest;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.l4digital.fastscroll.FastScrollRecyclerView;
import com.maxworld.contactslist.R;
import com.maxworld.contactslist.adapter.ContactAdapter;
import com.maxworld.contactslist.adapter.OnClick;
import com.maxworld.contactslist.model.Contact;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.maxworld.contactslist.config.Constants.CONTACTS_LOADER_ID;
import static com.maxworld.contactslist.config.Constants.REQUEST_CALL_PERMISSIONS;
import static com.maxworld.contactslist.config.Constants.REQUEST_CONTACTS_PERMISSIONS;

public class MainActivity extends AppCompatActivity implements OnClick, LoaderManager.LoaderCallbacks<Cursor> {

    @Bind(R.id.recyclerView) FastScrollRecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private ContactAdapter contactAdapter;
    private Contact lastContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, REQUEST_CONTACTS_PERMISSIONS);
        } else {
            initLoader();
        }
    }


    @Override
    public void onClick(Contact contact) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(String.format("tel:%s", contact.getPhone())));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            lastContact = contact;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PERMISSIONS);
            return;
        }
        startActivity(callIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CALL_PERMISSIONS:
                if ((grantResults.length > 0) &&
                        (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onClick(lastContact);
                } else {
                    Toast.makeText(this, R.string.no_call_permission, Toast.LENGTH_LONG).show();
                }
                break;

            case REQUEST_CONTACTS_PERMISSIONS:
                if ((grantResults.length > 0) &&
                        (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    initLoader();
                } else {
                    Toast.makeText(this, R.string.no_contacts_permission, Toast.LENGTH_LONG).show();
                }
                break;

            default:
                break;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == CONTACTS_LOADER_ID) {
            return contactsLoader();
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        setRecyclerView(contactsFromCursor(data));
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        contactAdapter.clear();
    }

    private void initLoader() {
        getLoaderManager().initLoader(CONTACTS_LOADER_ID, null, this);
    }

    private Loader<Cursor> contactsLoader() {
        Uri contactsUri = ContactsContract.Contacts.CONTENT_URI;

        String[] projection = {
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.HAS_PHONE_NUMBER
        };

        String selection = null;
        String[] selectionArgs = {};
        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME;

        return new CursorLoader(
                getApplicationContext(),
                contactsUri,
                projection,
                selection,
                selectionArgs,
                sortOrder);
    }

    private ArrayList<Contact> contactsFromCursor(Cursor cursor) {
        ArrayList<Contact> contacts = new ArrayList<>();

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String phone = "";

                if (cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor cursorInfo = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);

                    if (cursorInfo != null && cursorInfo.moveToNext()) {
                        phone = cursorInfo.getString(cursorInfo.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        cursorInfo.close();
                    }
                }

                contacts.add(new Contact(name, phone));
            } while (cursor.moveToNext());
        }

        return contacts;
    }


    public void setRecyclerView(ArrayList<Contact> contacts) {
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        contactAdapter = new ContactAdapter(contacts, this);
        recyclerView.setAdapter(contactAdapter);
    }

}
