package com.maxworld.contactslist.model;

public class Contact {

    private String name;
    private String phone;


    public Contact(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public String getText() {
        return String.format("%s / %s", name, phone);
    }


    public String getIndexText() {
        return name.substring(0, 1);
    }

    public String getPhone() {
        return phone;
    }
}
