package com.maxworld.contactslist.adapter;

import com.maxworld.contactslist.model.Contact;

public interface OnClick {
    void onClick(Contact contact);
}
