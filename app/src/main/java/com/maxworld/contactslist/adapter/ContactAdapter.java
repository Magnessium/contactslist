package com.maxworld.contactslist.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.l4digital.fastscroll.FastScroller;
import com.maxworld.contactslist.R;
import com.maxworld.contactslist.adapter.holder.ContactHolder;
import com.maxworld.contactslist.model.Contact;

import java.util.ArrayList;


public class ContactAdapter extends RecyclerView.Adapter<ContactHolder> implements FastScroller.SectionIndexer {

    private ArrayList<Contact> contacts = new ArrayList<>();
    private OnClick clickListener;

    public ContactAdapter(ArrayList<Contact> contacts, OnClick clickListener) {
        super();
        this.contacts = contacts;
        this.clickListener = clickListener;
    }

    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
        return new ContactHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ContactHolder holder, int position) {
        holder.bindData(contacts.get(position), getItemViewType(position), clickListener);
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2;
    }

    @Override
    public String getSectionText(int position) {
        return contacts.get(position).getIndexText();
    }

    public void clear() {
        contacts.clear();
    }
}
