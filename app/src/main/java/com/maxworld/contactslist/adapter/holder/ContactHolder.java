package com.maxworld.contactslist.adapter.holder;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.maxworld.contactslist.R;
import com.maxworld.contactslist.adapter.OnClick;
import com.maxworld.contactslist.model.Contact;


public class ContactHolder extends RecyclerView.ViewHolder {

    private TextView textView;
    private Contact contact;

    public void bindData(final Contact contact, int viewType, final OnClick clickListener) {
        this.contact = contact;
        textView.setText(contact.getText());
        textView.setBackgroundColor(viewType == 0 ? Color.WHITE : Color.GRAY);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.onClick(contact);
                }
            }
        });
    }

    public ContactHolder(View itemView) {
        super(itemView);
        textView = (TextView) itemView.findViewById(R.id.text);
    }
}
